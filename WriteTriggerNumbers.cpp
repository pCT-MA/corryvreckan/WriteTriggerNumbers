#include "WriteTriggerNumbers.h"

namespace corryvreckan {

    WriteTriggerNumbers::WriteTriggerNumbers(Configuration& config, std::vector<std::shared_ptr<Detector>> detectors)
        : Module(config, detectors), mTriggerNumber(0), mTree(nullptr) {}

    void WriteTriggerNumbers::initialize() {
        mTree = new TTree("triggerNumbers", "triggerNumbers");
        mTree->Branch("triggerNumber", &mTriggerNumber);
    }

    StatusCode WriteTriggerNumbers::run(std::shared_ptr<Clipboard> const& cb) {
        if(!cb->isEventDefined())
            return StatusCode::NoData;

        const auto& triggerList = cb->getEvent()->triggerList();
        if(triggerList.empty())
            return StatusCode::NoData;

        for(const auto& trigger : triggerList) {
            mTriggerNumber = trigger.first;
            mTree->Fill();
        }
        return StatusCode::Success;
    }

    void WriteTriggerNumbers::finalize(std::shared_ptr<ReadonlyClipboard> const&) {
        getROOTDirectory()->WriteTObject(mTree, "triggerNumbers");
        delete mTree;
        mTree = nullptr;
    }

} // namespace corryvreckan
