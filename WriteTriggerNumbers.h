#pragma once

#include "core/module/Module.hpp"

namespace corryvreckan {
    class WriteTriggerNumbers : public Module {
    public:
        WriteTriggerNumbers(Configuration& config, std::vector<std::shared_ptr<Detector>> detectors);
        virtual ~WriteTriggerNumbers() {}

        virtual void initialize() override;

        virtual StatusCode run(std::shared_ptr<Clipboard> const&) override;

        virtual void finalize(std::shared_ptr<ReadonlyClipboard> const&) override;

    private:
        uint32_t mTriggerNumber;
        TTree* mTree;
    };
} // namespace corryvreckan
